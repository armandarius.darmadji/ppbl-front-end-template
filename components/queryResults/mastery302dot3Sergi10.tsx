import { useQuery, gql } from "@apollo/client";

import {
    Flex, Center, Heading, Text, Box, Link, TableContainer, Table, Tbody, Td, Th, Thead, Tr
} from "@chakra-ui/react";

// ------------------------------------------------------------------------
// Module 302, Mastery Assignment #3
//
// STEP 1: Replace this GraphQL query with a new one that you write.
//
// Need some ideas? We will brainstorm at Live Coding.
// ------------------------------------------------------------------------
const QUERY = gql`
    query FatContractsV2{
        scripts(where:{type:{_eq:"plutusV2"}}, limit:10, order_by:{serialisedSize:desc}){
            type
            serialisedSize
            hash
            transaction {
            hash
            }
        }
        scripts_aggregate(where:{type:{_eq:"plutusV2"}}){
            aggregate{count}
        }
    }
`;

export default function Mastery302dot3Sergi10() {

    const queryAddress = "addr_test1wqunsl063ezhn67r72uxetvjugrmnnrkqu8pmd9exc6lcdcgnt2uf"

    // EXAMPLE WITH VARIABLE
    const { data, loading, error } = useQuery(QUERY, {

    });

    // EXAMPLE WITHOUT VARIABLE
    // const { data, loading, error } = useQuery(QUERY);

    if (loading) {
        return (
            <Heading size="lg">Loading data...</Heading>
        );
    };

    if (error) {
        console.error(error);
        return (
            <Heading size="lg">Error loading data...</Heading>
        );
    };

    // ------------------------------------------------------------------------
    // Module 302, Mastery Assignment #3
    //
    // STEP 2: Style your query results here.
    //
    // This template is designed be a simple example - add as much custom
    // styling as you want!
    // ------------------------------------------------------------------------

    return (
        <Box p="3" bg="orange.100" border='1px' borderRadius='lg'>
            <Heading py='2' size='md'>Mastery Assignment 302.3 by Sergi Estada</Heading>
            <Heading py='2' size='md'>Here are the top 10 largest scripts with Plutus V2 type</Heading>
            <Text p='1' fontWeight='bold'>Now in preprod testnet has {data.scripts_aggregate.aggregate.count}  Plutus V2 scripts</Text>
            <Box bg='grey' p='1'>
                <TableContainer bg='blue.200' p='1'>
                    <Table variant='simple'  fontSize='md'>
                        <Thead fontWeight='bold'>
                            <Tr bg='blue.300' color='white' >
                                <Th>Hash</Th>
                                <Th>Size</Th>
                            </Tr>
                        </Thead>
                        <Tbody p='10'>
                        {data.scripts.map((sc: any) =>(
                                <Tr>
                                    <Td><pre>{sc.hash}</pre></Td>
                                    <Td><pre>{sc.serialisedSize}</pre></Td>
                                </Tr>
                            ))}                          
                        </Tbody>
                    </Table>
                </TableContainer>
            </Box>
        </Box>
    )
}
